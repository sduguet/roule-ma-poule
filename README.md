# Hackathon React 2022

## Installation

```bash
npm install

npm start

# Dans un autre terminal
npm run server
```

## Connexion

### En tant qu'élève

Email:
```
eleve@eleve
```

Mot de passe:
```
eleve
```

### En tant que moniteur

Email:
```
moniteur@moniteur
```

Mot de passe:
```
moniteur
```
