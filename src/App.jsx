import React, { useEffect, useState } from 'react'
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from './pages/Home';
import UpcommingSessions from './components/UpcommingSessions';
import ShowInstructors from './components/ShowInstructors/ShowInstructors';
import Student from './components/Student/Student';
import MonitorStudent from './components/MonitorStudent/MonitorStudent';
import Layout from './components/Layout/Layout';
import CardMonitor from './components/CardMonitor/CardMonitor';
import Map from './components/Map';
import StudentPage from './pages/Student';
import RegisterNewRdv from './components/RegisterNewRdv/RegisterNewRdv';
import CoursSingle from './pages/Cours/Single';
import Monitors from './pages/Monitors';
import { AuthContext } from './lib/contexts/AuthContext';
import './styles/_reset.scss';
import './styles/_global.scss';
import EditRdv from './components/editRdv/EditRdv';
import AppMonitor from './pages/AppMonitor/AppMonitor';
import CoursList from './pages/CoursList';
import Cookies from 'js-cookie';

const App = () => {
  const [user, setUser] = useState(Cookies.get('user') ? JSON.parse(Cookies.get('user')) : null);

  useEffect(() => {
    Cookies.set('user', JSON.stringify(user));
  }, [user]);

  return (
    <BrowserRouter>
      <AuthContext.Provider value={{ user, setUser }}>
        <Layout>
          <Routes>
            <Route index={true} element={<Home/>}/>
            <Route path="sessions" element={<UpcommingSessions/>} />
            <Route path="show" element={<ShowInstructors/>}/>
            <Route path="map/:id" element={<Map/>}/>
            <Route path="student/:id" element={<Student />} />
            <Route path="monitor/:id" element={<CardMonitor />} />
            <Route path="student" element={<StudentPage/>}/>
            <Route path="monitors" element={<Monitors/>}/>
            <Route path="monitor-student/:id" element={<MonitorStudent />} />
            <Route path="editRdv/:id" element={<EditRdv />}/>
            <Route path="monitor" element={<AppMonitor />}/>
            <Route path="cours-list" element={<CoursList />} />
            <Route path="register" element={<RegisterNewRdv />} />
            <Route path="cours/:id" element={<CoursSingle />} />
          </Routes>
        </Layout>
      </AuthContext.Provider>
    </BrowserRouter>
  );
};
export default App;
