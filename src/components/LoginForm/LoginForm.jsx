import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styles from './LoginForm.module.scss';

const LoginForm = props => {

    const {
        onSubmit,
        changeHasAccount
    } = props


    const [formState, setFormState] = useState({
        email: '',
        password: ''
    })

    const handleFormChange = e => {
        setFormState({
            ...formState,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = evt => {
        evt.preventDefault()
        return onSubmit(formState)
    }

    return <form className={styles.form} onSubmit={handleSubmit}>
    <label className={styles.label} htmlFor="email">Adresse e-mail</label>
    <input
        className={styles.input} 
        name="email" 
        type="email" 
        placeholder="Entrez votre e-mail" 
        value={formState.email} 
        onChange={handleFormChange}
    />
    <label className={styles.label} htmlFor="password">Entrer votre mot de passe</label>
    <input
        className={styles.input} 
        name="password" 
        type="password" 
        placeholder="Mot de passe" 
        value={formState.password}
        onChange={handleFormChange}
    />
    <button className={styles.button}>Se connecter</button>
    <p className={styles.subbutton} onClick={changeHasAccount}>Vous n'avez pas encore de compte ?</p>
    </form>
}

LoginForm.propTypes = {
    disabled: PropTypes.bool,
    onSubmit: PropTypes.func
}

export default LoginForm
