import React, { useState, useEffect } from 'react'
import fullStar from '../../../assets/icons/fullstar.svg';
import demiStar from '../../../assets/icons/demistar.svg';
import voidStar from '../../../assets/icons/voidstar.svg';
import styles from './DashboardInstructor.module.scss';
import { Link } from "react-router-dom";

const DashboardInstructor = (props) => {

    const {
        idUser
    } = props

    const calcNote = (notes) => {
        let res = 0;
        if (notes) {
            let array = Array.from(Object.values(notes));
            for (let i = 0; i < array.length; i += 1) {
                res += array[i];
            }
            return (res / array.length)
        }

    }

    const [instructor, setInstructor] = useState([])
    useEffect(() => {
        fetch('/api/users/' + idUser)
            .then(res => res.json())
            .then(data => setInstructor(data))

    }, [idUser]);

    const notes = calcNote(instructor.notes)

    const [sessions, setSessions] = useState([])
    useEffect(() => {

        fetch('/api/sessions/')
            .then(res => res.json())
            .then(data => setSessions(data))
    }, []);
    let nextSession = ''
    sessions.filter(session => session.instructorUserId === idUser).map(session =>
        nextSession = session
    )

    const [places, setPlaces] = useState([])
    useEffect(() => {

        fetch('/api/places/')
            .then(res => res.json())
            .then(data => setPlaces(data))
    }, []);
    let nextPlace = ''
    places.filter(place => place.id === nextSession.placeId).map(place =>
        nextPlace = place
    )

    const [students, setStudents] = useState([])
    useEffect(() => {

        fetch('/api/users/')
            .then(res => res.json())
            .then(data => setStudents(data))
    }, []);
    let nextStudent = ''
    students.filter(student => student.id === nextSession.studentUserId).map(student =>
        nextStudent = student
    )
    let likes = 0
    students.filter(student => student.favoriteInstructorsId.includes(instructor.id)).map(instructor =>
        likes += 1
    )

    // config pour le format de la date
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    const opt_weekday = { weekday: 'long' };
    function toTitleCase(str) {
        return str.replace(
            /\w\S*/g,
            function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
        );
    }

    let progress = ''
    if (nextStudent.length !== 0) {
        progress = nextStudent.competences.length
    }

    return <div className={styles.content}>
        <h3 className={styles.title}>Bonjour, {instructor.firstName} {instructor.lastName}</h3>
        <div>
            <p className={styles.subtitle}>Mon prochain cours :</p>

            {
                nextStudent !== '' ?
                    <section className={styles.card}>
                        <p>{new Date(nextSession.dateStart).toLocaleTimeString().slice(0, 2)}h - {new Date(nextSession.dateEnd).toLocaleTimeString().slice(0, 2)}h le {toTitleCase(new Date(nextSession.dateEnd).toLocaleDateString("fr-FR", opt_weekday))} {new Date(nextSession.dateStart).toLocaleTimeString("fr-FR", options).slice(0, -15)}</p>
                        <p>{nextPlace.name}, 17000 La Rochelle</p>
                        <Link to={'/monitor-student/'+ nextStudent.id} className={styles.student}><p>avec {nextStudent.firstName} {nextStudent.lastName}</p></Link>
                        
                        {
                            progress !== '' ? <div className={styles.progress}><progress id="file" max="90" value={progress * 10}></progress><p>{progress * 10}%</p> </div>: ''
                        }
                    </section>
                    :
                    <section>
                        <p>Pas de prochain cours</p>
                    </section>
            }
        </div>


        <section className={styles.info}>
            <p className={styles.subtitle}>Mes statistiques</p>
            <div className={styles.infoContent}>
                <div className={styles.stat}>
                    <div className={styles.likes}>
                        <p className={styles.like}>{likes}</p>
                        <img src={`${process.env.PUBLIC_URL}/heart.svg`} alt="" />
                    </div>
                    <div className={styles.likes}>
                        <p className={styles.heure}>53</p>
                        <img src={`${process.env.PUBLIC_URL}/clock.svg`} alt="" />
                    </div>
                </div>
                <div>
                    <p className={styles.avis}>Avis :</p>
                    <ul className={styles.stat}>
                        {[...Array(5)].map((x, index) => {
                            let src = '';
                            if (notes <= index) {
                                src = voidStar;
                            } else if (notes > index + 0.5) {
                                src = fullStar;
                            } else {
                                src = demiStar;
                            }
                            return (
                                <li key={index}>
                                    <img src={src} alt="" />
                                </li>
                            );
                        })}
                    </ul>
                </div>
            </div>
        </section>
    </div>
}

export default DashboardInstructor