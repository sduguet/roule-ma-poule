import React, {useState, useEffect} from 'react'
import DashboardStudent from './Childs/DashboardStudent';
import Infos from './Childs/Infos';
import DashboardInstructor from './Childs/DashboardInstructor';
import styles from './ProfilTabs.module.scss';

const ProfilTabs = (props) => {

    const {
        idUser
    } = props

    const [user, setUser] = useState([])
    useEffect(() => {
        fetch('/api/users/' + idUser)
            .then(res => res.json())
            .then(data => setUser(data))
    }, [idUser]);

    const userType = user.role

    const [currentTab, setCurrentTab] = useState(1)

    return  <div className={styles.tabs}>
                <nav className={`${styles.nav} ${userType}`}>
                    <ul className={styles.navList}>
                        <li className={`${styles.navListEle} ${currentTab === 1 ? 'active' : ''}`} onClick={() => setCurrentTab(1)}>Tableau de bord</li>
                        <li className={`${styles.navListEle} ${currentTab === 2 ? 'active' : ''}`} onClick={() => setCurrentTab(2)}>Mes informations</li>
                    </ul>
                </nav>

                <div className={`${styles.window} ${userType}`}>
                    {
                        currentTab === 1 ?  
                            userType === 'student' ?
                                <DashboardStudent idUser={idUser}/>
                            :
                            <DashboardInstructor idUser={idUser}/>
                        :
                            ''
                    }
                    {
                        currentTab === 2 ? <Infos idUser={idUser} userType={userType}/> : ''
                    }
                </div>
                
            </div>
}

export default ProfilTabs