import React, { useEffect, useState } from "react";
import styles from './Revenus.module.scss';

//APPEL DU COMPOSANT : <Route path="revenus" element={<Revenus idUser={} />} />

const Revenus = props => {
    const {
        idUser,
    } = props


    const [totalHours, setTotalHours] = useState(0)
    useEffect(() => {
        fetch('/api/sessions?instructorUserId=' + idUser)
            .then(res => res.json())
            .then(data => {
                // setSessions(Array.from(Object.values(data))))
                const totalHours = data.reduce((total, session) => {
                    return total + (session.dateEnd - session.dateStart)
                }, 0) / 3600000
                setTotalHours(totalHours)
            },
            )
    }, [idUser])

    return <div>{totalHours && <>
        <div className={styles.revenus}>
            <div className={styles.revenus__wrapper}>
                <h2 className={styles.revenus__titre}>Vos revenus globaux :</h2>
                <p className={styles.revenus__montant}>{totalHours * 40} €</p>
                <span className={styles.revenus__prixSceance}>sur une base de 40€/heure de scéance</span>
                <div className={styles.cercle}></div>
            </div>
        </div>
    </>}
    </div>
}
export default Revenus