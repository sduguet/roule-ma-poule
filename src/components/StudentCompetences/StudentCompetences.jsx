import React, { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { AuthContext } from '../../lib/contexts/AuthContext';
import styles from "./StudentCompetences.module.scss"

const StudentCompetences = () => {
    const { user: student } = useContext(AuthContext);
    // const {id} = useParams()
    // const [student, setStudent] = useState(null)
    const [competences, setCompetences] = useState(null)
    const [categories, setCategories] = useState(null)
    // useEffect(() => {
    //     fetch('/api/users/' + id)
    //         .then(res => res.json())
    //         .then(data => setStudent(data))
    // }, [id])

    useEffect(() => {
        fetch('/api/competences')
            .then(res => res.json())
            .then(data => {
              setCompetences(data);
            })
    }, [])

    useEffect(() => {
        fetch('/api/categoriesCompetences')
            .then(res => res.json())
            .then(data => setCategories(data))
    }, [])

    const hasCpt = id => {
        console.log(student)
        for (let i = 0; i < student.competences.length; i++) {
            if (student.competences[i] === id) {
                return true
            }
        }
    }
    return <div className={styles.competences}>{student && competences && categories && <>
        {categories.map(categorie =>
            <ul className={styles.competences__list} key={categorie.id}>
                <h3 className={styles.competences__category}>{categorie.label}</h3>
                {competences.filter(cpt => cpt.categoryId === categorie.id).map(cpt =>
                    <li className={styles.competences__cpt} key={cpt.id}>
                        <input type={"checkbox"} defaultChecked={hasCpt(cpt.id)} />
                        <label htmlFor="1" className={styles.competences__label}>{cpt.label}</label>
                    </li>)}
            </ul>
        )}
    </>}
    </div>
}
export default StudentCompetences