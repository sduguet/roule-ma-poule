import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../lib/contexts/AuthContext";
import styles from "./AppMonitor.module.scss";
import Revenus from '../../components/Revenus/Revenus';
import RegisterNewRdv from "../../components/RegisterNewRdv/RegisterNewRdv";
import { useNavigate, Link } from "react-router-dom";
import ProfilTabs from '../../components/ProfilsTabs/ProfilTabs';

const AppMonitor = () => {
    const {user} = useContext(AuthContext);
    const [sessions, setSessions] = useState({});
    const [places, setPlaces] = useState([]);
    const [students, setStudents] = useState([]);
    const navigate = useNavigate();

    const searchPlaces = (id) => {
        for (let i = 0; i < places.length; i += 1) {
            if (places[i].id === id) {
                return places[i].name
            }
        }
    }

    const searchStudent = (id) => {
        for (let i = 0; i < students.length; i += 1) {
            if (students[i].id === id) {
                return `${students[i].firstName} ${students[i].lastName}`;
            }
        }
    }

    const erase = (id) => {
        console.log('yes');
        fetch('/api/sessions/'+id, {method: 'DELETE'})
            .then(res => res.json())
            .then(data => {
                setSessions(sessions.filter(session => session.id !== id))
            }) 
    }

    const checkPlaces = () => {
        fetch('/api/places')
            .then(res => res.json())
            .then(dataPlaces => setPlaces(dataPlaces))    
    }

    const checkStudents = () => {
        fetch('/api/users')
            .then(res => res.json())
            .then(dataStudents => setStudents((dataStudents)))    
    }


    useEffect(() => {
        fetch('/api/sessions?instructorUserId=' + user.id)
            .then(res => res.json())
            .then(dataSession => setSessions(dataSession))     
        checkPlaces()
        checkStudents()
        if (!user.id) {
            navigate('/');
        }    
    }, [user.id])

    return <div className={styles.monitor}>
        <section>
        <ProfilTabs idUser={user.id}/>
      </section>
        <h2 className={styles.subtitle}>Cours à venir</h2>
        <ul className={styles.array}>{Array.from(sessions)
              .sort((a, b) => a.timestamp - b.timestamp)
              .slice(0, 3)
              .map(session => <li className={styles.cours} key={session.id}>
                  <p className={styles.date}>Le {parseInt(new Date(session.dateStart).getDate()) < 10 ? ('0'+(new Date(session.dateStart).getDate())) : (new Date(session.dateStart).getDate())}/{parseInt(new Date(session.dateStart).getMonth()) < 10 ? ('0'+(new Date(session.dateStart).getMonth())) : (new Date(session.dateStart).getMonth())}/{new Date(session.dateStart).getFullYear()} à {parseInt(new Date(session.dateStart).getHours()) < 10 ? ('0'+(new Date(session.dateStart).getHours())) : (new Date(session.dateStart).getHours())}h{parseInt(new Date(session.dateStart).getMinutes()) < 10 ? ('0'+(new Date(session.dateStart).getMinutes())) : (new Date(session.dateStart).getMinutes())}</p>
                  <p className={styles.place}>{searchPlaces(session.placeId)}</p>
                  {session.studentUserId !== null && <Link to={'/monitor-student/'+session.studentUserId} className={styles.student}><p className={styles.student}>{searchStudent(session.studentUserId)}</p></Link>}
                  {session.studentUserId == null && <p className={styles.student}>{"Pas d'élèves"}</p>}
                  <div className={styles.button}>
                    <Link to={'/editRdv/'+session.id}>Modifier</Link>
                    <button onClick={() => {erase(session.id)}}>Supprimer</button>
                  </div>
              </li>)}
        </ul>
        <h2 className={styles.subtitle}>Créer un nouveau créneau</h2>
        <RegisterNewRdv idUser={user.id}></RegisterNewRdv>
        <h2 className={styles.subtitle}>Revenus</h2>
        <Revenus idUser={user.id}/>  
    </div>
}

export default AppMonitor
