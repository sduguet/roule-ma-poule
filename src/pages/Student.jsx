import React, { useContext } from "react";
import { AuthContext } from "../lib/contexts/AuthContext";
import Reservation from '../components/reservation/Reservation';
import FavoriteIntructor from '../components/favoriteInstructor/FavoritInstructors';
import styles from '../styles/pages/Student.module.scss';
import StudentCompetences from '../components/StudentCompetences/StudentCompetences';
import ProfilTabs from '../components/ProfilsTabs/ProfilTabs';
import { Link } from 'react-router-dom';

const Student = () => {
  const {user} = useContext(AuthContext);

  return ( <>
      <section>
        <ProfilTabs idUser={user.id}/>
      </section>
    <main className={styles.main}>
      <section className={styles.content}>
        <h2 className={styles.subtitle}>Mes réservations :</h2>
        <Reservation />
      </section>
      <div className={styles.link}>
        <span className={styles.line} />
        <Link className={styles.text} to="/cours-list">Réserver un cours <img className={styles.icon} src={`${process.env.PUBLIC_URL}/icon-chevron-rights.png`} alt="chevron" /></Link>
      </div>
      <section className={styles.content}>
        <h2 className={styles.subtitle}>Mes moniteurs favoris : </h2>
        <FavoriteIntructor />
      </section>
      <div className={styles.link}>
        <span className={styles.line} />
        <Link className={styles.text} to="/monitors">Voir d’autres moniteurs <img className={styles.icon} src={`${process.env.PUBLIC_URL}/icon-chevron-rights.png`} alt="chevron" /></Link>
      </div>
      <section className={styles.content}>
        <h2 className={styles.subtitle}>Mes compétences : </h2>
        <StudentCompetences />
      </section>
    </main></>
  );
};

export default Student;
